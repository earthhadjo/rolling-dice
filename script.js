// 2 random dice roles
function rollDice() { 
    let die1=Math.ceil(Math.random()*6);
    let die2=Math.ceil(Math.random()*6);
    let rollTotal=die1+die2;
    return rollTotal;
}

// arrays
let possible = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
let count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// 1000x Dice Roll
for (i=1; i<=1000; i++) {
    let result = rollDice();
    count[result] = count[result] + 1;
}

//Output
let destination = document.getElementById('chart-container')
let widthMultipler = 2

for (let j = 2; j < count.length; j++) {

    // layout
    let chartRowElement = document.createElement('div')
    chartRowElement.style.display = "flex"
    
    // Header
    let headerElement = document.createElement('div')
    headerElement.style.width = "32px"
    let headerText = document.createTextNode(possible[j-2])
    headerElement.appendChild(headerText)
    chartRowElement.appendChild(headerElement)
    
    //Some style
    let barElement = document.createElement('div')
    barElement.style.backgroundColor = 'purple'
    barElement.style.width = count[j]*widthMultipler+'px'
    let divText = document.createTextNode(count[j])
    barElement.appendChild(divText)
    chartRowElement.appendChild(barElement)
    destination.appendChild(chartRowElement)
}

